module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "script"
    },
    "plugins": [
    ],
    "rules": {
        "no-unused-vars": [
            "warn", 
            {
                "args": "after-used",
                "varsIgnorePattern": "_+"
            }
        ],
        "eqeqeq": [
            "error",
            "always"
        ],
        "strict": [
            "error",
            "global"
        ],
        "no-console": 0,
        "no-else-return": 2,
        "no-loop-func": 2,
        "linebreak-style": [
            "error",
            "unix"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
