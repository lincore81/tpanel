"use strict";
require('babel-polyfill');
const {m, mount} = require('./mount');
const utils = require('./utils');
const style = require('./style');
const view = require('./view');
//const transcript = require('./transcript');
const MutationObserver = require('mutation-observer');

const DEFAULT_PANEL_HEIGHT = '12em';

const elementGetters = [
    ['transcript', 'section#panel-transcript'],
    ['outline', 'section#panel-outline'],
    ['outlineLabel', '#tab-outline'],
    ['transcriptLabel', '#tab-transcript'],
    ['outlineTab',
        () => {
            const label = document.getElementById('tab-outline');
            return label && label.parentElement;
        }],
    ['transcriptTab',
        () => {
            const label = document.getElementById('tab-transcript');
            return label && label.parentElement;
        }],
    ['tabs', '.tabs'],
    ['slideContainer', '.slide-container']
];

window.addEventListener('load', () => utils.observe(
            document.querySelector.bind(document, 'section#panel-transcript'), init, 500));

function init() {
    console.log('init');
    const ctx = getElements();
    window._ctx = ctx;
    utils.addStyleSheet(document, style());
    ctx.panel = createPanel(ctx.slideContainer, ctx.transcript, DEFAULT_PANEL_HEIGHT);
    replaceTabs(ctx);
    replaceGlobalProvieData(onGlobalData.bind(null, ctx));
    //const mo = new MutationObserver(onTranscriptMutation.bind(null, ctx));
    //mo.observe(ctx.transcript, {childList: true, subtree: true});
}

function getElements() {
    return utils.toObject(elementGetters.map(([k,v]) => {
        switch (typeof v) {
            case 'string': 
                v = document.querySelector(v); 
                break;
            case 'function': 
                v = v(); 
                break;
            default: 
                throw new Error('Invalid getter: ' + v);
        } 
        if (!v) throw new Error('Unable to get element: ' + k);
        return [k,v];
    }));
}
            

function onGlobalData(ctx, ev, json) {
    if (ev === 'frame') return onFrame();
    if (ev === 'slide') return onSlide();
    console.log(ev, json);

    function onFrame() {
        ctx.notesData = {};
        for (let x of json.notesData) {
            const key = x.slideId.replace(/^\w+\./, '');
            const value = x.content;
            ctx.notesData[key] = value;
        }
    }

    function onSlide() {
        ctx.panel['tp-title'].innerText = json.title;
        const notes = ctx.notesData[json.id] || '';
        ctx.panel['tp-transcript'].innerHTML = notes;
    }
}



// Create custom outline and transcript tabs and hide the original ones.
// This is done to circumvent existing click event listeners.
function replaceTabs(ctx) {
    const notesStr = ctx.transcriptLabel.innerText;
    const outlineStr = ctx.outlineLabel.innerText;
    const tabs = mount(view.htmlTabs(outlineStr, notesStr), ctx.tabs.parentElement, true);
    ctx.tabs.style.display = "none";
    tabs['tp-transcript-tab'].addEventListener('click', toggleTranscriptPanel.bind(null, ctx));
    ctx.altTabs = tabs;

    // make storyline think the transcript tab is selected but show the menu instead:
    ctx.transcriptLabel.click();
    utils.removeClass(ctx.outline, 'hidden');
    //ctx.outline.style.display = "initial";
}

function createPanel(slide, transcript, defaultHeight) {
    const nodes = view.htmlTranscriptPanel(transcript, defaultHeight);
    const elems = mount(nodes, slide);
    utils.makeResizable(elems['tp-panel'], elems['tp-panel-resize-handle'], validatePanelSize);
    utils.makeEthereal(elems['tp-panel'], elems['tp-panel'], 0.85, 1);
    return elems;

    function validatePanelSize(size) {
        const panel = elems['tp-panel'];
        const style = document.defaultView.getComputedStyle(panel.parentElement);
        const maxHeight = parseInt(style.height, 10) * 0.8;
       
        size.x = -1;
        if (size.y < 8) size.y = 8;
        if (size.y >= maxHeight) size.y = maxHeight;
        return size;
    }
}

function toggleTranscriptPanel(ctx) {
    const panel = ctx.panel['tp-panel'];
    const transcriptTab = ctx.altTabs['tp-transcript-tab'];
    const showPanel = utils.hasClass(panel, 'tp-hidden');
    if (showPanel) {
        utils.removeClass(panel, 'tp-hidden');
        utils.addClass(transcriptTab, 'cs-selected active');
    } else {
        utils.addClass(panel, 'tp-hidden');
        utils.removeClass(transcriptTab, 'cs-selected');
        utils.removeClass(transcriptTab, 'active');
    }     
}

function replaceGlobalProvieData(callback) {
    const oldGlobalProvideData = window.globalProvideData;
    window.globalProvideData = (ev, json) => {
        callback(ev, JSON.parse(json));
        if (ev !== 'frame') {
            oldGlobalProvideData(ev, json);
        }
    };
    mount(m('script', {src: 'html5/data/js/frame.js'}), document.body);
}
