"use strict";
const marked = require('marked');
module.exports = {toHtml};

/*
 * this module converts Storyline's SVG transcript into HTML.
 */

//                 $1                                               $2
const REGEX_UL = /^(\s*)_*[\u2022\u2023\u25e6\u2043\u2219\oO.*-]_*(\s+)/gm;
//                 $1     $2                                 $3
const REGEX_OL = /^(\s*)_*(\d+|[IVXLCDM]+|[A-Za-z])(?:[):.])_*(\s+)/gm;

function toHtml(svg) {
    const textElems = Array.from(svg.getElementsByTagName('text'));
    const tokens = textElems
        .map(convertTextElem)
        .sort((a, b) => a.y - b.y || a.x - b.x);
    //console.log('tokens:', tokens);
    addLinebreaks(tokens);
    applyFormat(tokens);
    const md = tokens
        .map(t => t.text)
        .join('')
        .replace(REGEX_UL, '$1-$2')
        .replace(REGEX_OL, '$1$2.$3');
    //console.log('markdown:', md);
    return marked(md);
}

/** 
 * Create and return a token from the given object
 */
function token({type, text, x, y, height, font, fontSize, color}) {
    if (typeof arguments[0] === 'string') return {type: arguments[0]};
    const t = {text, x, y, height, font, fontSize, color};
    t.type = type || 'literal';
    t.bold = /bold/i.test(font);
    t.italic = /italic/i.test(font);
    t.style = (t.bold? 2 : 0) + (t.italic? 1 : 0);
    return t;
}

/**
 * Convert a <text><tspan /></text> structure into a token:
 */
function convertTextElem(textElem) {
    const children = Array.from(textElem.childNodes);
    if (children.length === 0) {
        throw new Error('text token should have exactly one child, but has none.', textElem);
    } else if (children.length > 1) {
        throw new Error('text token should have exactly one child, but has more.', textElem);
    }
    if (children[0].tagName.toLowerCase() !== 'tspan') {
        throw new Error('text token\'s child is not a tspan.', textElem);
    }
    const tspan = children[0];
    return token({
        text: tspan.textContent,
        x: parseInt(tspan.getAttribute('x')),
        y: parseInt(tspan.getAttribute('y')),
        height: textElem.getBBox().height,
        font: textElem.getAttribute('font-family'),
        fontSize: textElem.getAttribute('font-size'),
        color: textElem.getAttribute('fill')
    });
}

function addLinebreaks(tokens) {
    tokens.reduce((a, b) => {
        const n = getLineDiff(a, b);
        if (n > 0) a.text += '\n'.repeat(n);
        return b;
    });

    function getLineDiff(a, b) {
        if (a.y === b.y) return 0;
        if (!a.height) return 1;
        const diff = b.y - a.y;
        return Math.floor(diff / a.height);
    }
}

/*
 * 0 n = n
 * n 0 = 0
 * 3 n = n
 * n 3 = 3
 * 1 2 = 1 2
 * 2 1 = 2 1
 */
function applyFormat(tokens) {
    const empty = () => {return {text: '', style: 0};};
    tokens.unshift(empty());
    tokens.push(empty());
    tokens.reduce((a, b) => {
        const emphasis = b.style - a.style;
        if (emphasis === 0) return b;
        if (a.style === 1 && b.style === 2 || a.style === 2 && b.style === 1) {
            a.text = appendMarkup(a.text, '_'.repeat(a.style));
            b.text = prependMarkup(b.text, '_'.repeat(b.style));
        } else {
            const markup = '_'.repeat(Math.abs(emphasis));
            if (emphasis > 0) {
                b.text = prependMarkup(b.text, markup);
            } else if (emphasis < 0) {
                a.text = appendMarkup(a.text, markup);
            }
        }
        return b;
    });

}

function appendMarkup(text, markup) {
    const match = text.match(/\s+$/);
    if (!match) return text + markup;
    const 
        a = text.substring(0, match.index),
        b = text.substring(match.index);
    return a + markup + b;
}

function prependMarkup(text, markup) {
    const match = text.match(/\S/);
    if (!match) return markup + text;
    const 
        a = text.substring(0, match.index),
        b = text.substring(match.index);
    return a + markup + b;
}
