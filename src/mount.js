"use strict";
module.exports = {m, mount};


// Create a tree of 'virtual' nodes from HTML, which can later be converted 
// to actual HTMLElements.
function m(tag, attributes) {
    if (typeof tag !== 'string') {
        // assume it's a html element
        return {element: tag};
    }
    const node = {tag: tag, attributes: attributes};
    node.children = Array.from(arguments).slice(2);
    return node;        
}
// Create HTMLElements from 'virtual' nodes and append or prepend everything 
// to the given element. Return a map of all created elements with a unique id,
// such that they can be accessed using its id as key.
function mount(what, where, prepend) {
    const elems = {};
    makeElem(where, what, prepend);
    return elems;

    // recursive:
    function makeElem(parent, node, prepend) {
        if (typeof node === 'string') {
            makeText(parent, node);
        }
        if (node.element) {
            parent.appendChild(node.element);
        }

        const elem = document.createElement(node.tag);
        if (node.attributes) setAttribs(node.attributes, elem);
        if (prepend && parent.firstChild) {
            parent.insertBefore(elem, parent.firstChild);
        } else {
            parent.appendChild(elem);
        }
        if (!node.children || node.children.length === 0) return; 
        node.children.forEach(function(child) {
            makeElem(elem, child);
        });
    }

    // all texts are leaves, no recursion here:
    function makeText(parent, text) {
        const node = document.createTextNode(text);
        parent.appendChild(node);
    }

    function setAttribs(attribs, elem) {
        Object.keys(attribs).forEach(function(key) {
            const value = attribs[key];
            elem.setAttribute(key, value);
            if (key === 'id') elems[value] = elem;
        });
    }
}

