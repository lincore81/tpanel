"use strict";
const {m} = require('./mount');

module.exports = {
    htmlTranscriptPanel,
    htmlTabs
};

/////// HTML VIEW ///////

function htmlTranscriptPanel(transcript, defaultHeight) {
    return m('div', {
            id: "tp-panel", 
            class: 'tp-hidden', 
            'aria-hidden': 'true',
            style: 'top: 0; left: 0; right: 0; height: ' + defaultHeight}, 
        htmlWindowContent(transcript),
        htmlPanelResizeHandle());
}

function htmlPanelResizeHandle() {
    return m('div', {id: 'tp-panel-resize-handle'}, 
            m('img', {
                src: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='
            }));
}

function htmlWindowContent() {
    return m('div', {id: "tp-content"}, 
             m('div', {id: "tp-scroll-pane"}, 
               m('div', {id: "tp-text-wrapper"},
                 m('h4', {id: 'tp-title'}),
                 m('hr', {class: "tp-hr"}),
                 m('div', {id: 'tp-transcript'}) )));
}

function htmlTabs(outlineLabel, transcriptLabel) {
    return m('ul', {class: "tabs", role: "presentation"},
        htmlTab(outlineLabel, 'tp-outline-tab', 'tp-outline-btn', 'cs-selected active'),
        htmlTab(transcriptLabel, 'tp-transcript-tab', 'tp-transcript-btn'));
}

function htmlTab(label, liId, btnId, classes) {
    return m('li', {
            class: "tp-tab tab cs-tabs " + (classes || ''), 
            tabindex: "-1", 
            role: "button", 
            id: liId}, 
        m('button', {class: "tabs-trigger cs-inner-button", id: btnId}, label));
}
