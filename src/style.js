"use strict";
module.exports = getStyle;

// Return the style sheet to use, so that everything fits neatly into
// one file.
function getStyle() {
    return `
        section#panel-transcript {
            position: absolute;
            z-index: -10000;
            opacity: 0;
            display: -ms-flexbox !important;
            display: -webkit-flex !important;
            display: -webkit-box !important;
            display: -moz-flex !important;
            display: flex !important;
            display: unset !important;
        }
        section#panel-outline {
            display: -ms-flexbox !important;
            display: -webkit-flex !important;
            display: -webkit-box !important;
            display: -moz-flex !important;
            display: flex !important;
            display: unset !important;
            overflow-y: scroll;
        }
        .tp-tab {
            outline: none;
        }
        #tp-panel {
            z-index: 1000000;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
        }
        #tp-panel-resize-handle {
            position: absolute;
            height: 8px;
            bottom: -4px;
            left: 0;
            right: 0;
            cursor: ns-resize;
        }
        #tp-panel-resize-handle > img {
            width: 100%;
            height: 100%;
        }
        #tp-wrapper {
            position: relative;
        }
        #tp-title {
            margin: 0;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        hr.tp-hr {
            border-color: gainsboro;
            position: relative;
            top: 1em;
        }
        #tp-content {
            position: relative;
            height: 100%;
            line-height: 1.308;
        }
        #tp-scroll-pane {
            height: 100%;
            overflow-y: scroll;
            background: white;
            box-shadow: 0 4px 4px rgba(0,0,0, 0.3);
            border-bottom: 1px solid #333;
        }
        #tp-text-wrapper {
            padding: 8px 8px 0px 8px;
            font-size: 1.1em;
        }
        #tp-resize-handle {
           background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAALUlEQVR42mNgwAH279//H4QZyFYAAv///5fBqQurCQQVEHQMVjaMAQQc2BQAABXMU79BvB5bAAAAAElFTkSuQmCC") no-repeat center center;
           border: 2px solid transparent;
           cursor: nwse-resize;
           position: absolute;
           bottom: 0px;
           right: 0px;
           width: 8px;
           height: 8px;
           z-index: 100;
        }
        .tp-ethereal {
            opacity: 0.85;
        }
        .tp-hidden {
            display: none !important;
        }`;
}

