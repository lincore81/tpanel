"use strict";
module.exports = {
    makeResizable,
    makeEthereal,
    addStyleSheet,
    getElementByClassName,
    hasClass,
    addClass,
    removeClass,
    observe,
    toObject
};

///////// UTILS ///////// 

// Enable a given HTMLElement to be resized by the user via click and drag.
// The element's boundaries can not exceed those of the current viewport.
function makeResizable(elem, handle, sizeValidator) {
    var startX, 
        startY, 
        startSize,
        startWidth, 
        startHeight;
    handle.addEventListener('mousedown', initDrag, false);
    
    function initDrag(e) {
        e.preventDefault();
        startX = e.clientX;
        startY = e.clientY;
        startSize = {x: startX, y: startY};
        startWidth = parseInt(document.defaultView.getComputedStyle(elem).width, 10);
        startHeight = parseInt(document.defaultView.getComputedStyle(elem).height, 10);
             
        document.documentElement.addEventListener('mousemove', doDrag, false);
        document.documentElement.addEventListener('mouseup', stopDrag, false);
    }

    function doDrag(e) {
        e.preventDefault();
        var newSize = {
            x: startWidth + e.clientX - startX,
            y: startHeight + e.clientY - startY
        };
        var validSize = sizeValidator(newSize, startSize);
        if (!validSize) return;
        if (validSize.x >= 0) elem.style.width = validSize.x + 'px';
        if (validSize.y >= 0) elem.style.height = validSize.y + 'px';
    }
    function stopDrag(e) {
        e.preventDefault();
        document.documentElement.removeEventListener('mousemove', doDrag, false);
        document.documentElement.removeEventListener('mouseup', stopDrag, false);
    }
}


// make the given element semi-transparent while the mouse is not over it
function makeEthereal(elem, initiallyTransparent) {
    var clazz = 'tp-ethereal';  
    if (initiallyTransparent) set(true);
    elem.addEventListener('mouseenter', set.bind(null, false));
    elem.addEventListener('mouseleave', set.bind(null, true));

    function set(state) {
        if (state) {
            if (!hasClass(elem, clazz)) addClass(elem, clazz);
        } else {
           removeClass(elem, clazz);
        }
    }
}

/*
function observeTextChange(element, callback, context) {
    callback = callback.bind(null, context);
    var Observer = window.MutationObserver || window.WebkitMutationObserver;
    return Observer? newschool() : oldschool();

    function newschool() {
        var mo = new Observer(callback);
        mo.observe(element, {childList: true, subtree: true});
        return mo.disconnect.bind(mo);
    }
    // for IE 9, 10
    function oldschool() {
        var text = element.innerText;
        var id = setInterval(ontime, 500);
        return clearInterval.bind(window, id);

        function ontime() {
            if (text === element.innerText) return;
            callback();
            text = element.innerText;
        }
    }
}*/

/**
 * Creates and appends a new style-node to the given document's head.
 * @param doc the document to append to
 * @param rules {string} the style-tag's content
 * @returns the created style-element
 */ 
function addStyleSheet(doc, rules) {
    const css = doc.createElement('style');
    css.type = 'text/css';
    if (css.styleSheet) {
        css.styleSheet.cssText = rules;
    } else {
        css.appendChild(doc.createTextNode(rules));
    }
    doc.getElementsByTagName("head")[0].appendChild(css);
    return css;
}

function getElementByClassName(classname) {
    const elems = document.getElementsByClassName(classname);
    return elems[0];
}

function hasClass(elem, classname) {
    const xs = classname.split(' ');
    const ys = elem.className.split(' ');
    return xs.every(x => ys.some(y => x === y));
}

// does not check whether the elem aready has the class!
function addClass(elem, classname) {
    if (elem.className) elem.className += ' ';
    elem.className += classname;
}

function removeClass(elem, classname) {
    elem.className = elem.className.split(' ').filter(function(cn) {
        return cn !== classname;
    }).join(' ');
}


function observe(predicate, callback, interval) {
    const id = setInterval(onTime, interval);
    onTime();
    return id;

    function onTime() {
        const ans = predicate();
        if (!ans) return;
        clearInterval(id);
        callback(ans);
    }
}

function toObject(array) {
    const obj = {};
    array.forEach(([k,v]) => obj[k] = v);
    return obj;
}


/*    function fireEvent(elem, type){
    if (elem.fireEvent) {
        elem.fireEvent('on' + type);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(type, true, false);
        elem.dispatchEvent(evObj);
    }
}*/




